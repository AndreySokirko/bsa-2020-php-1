<?php

declare(strict_types=1);

namespace App\Task1;
use App\Task1\CarAdd;

class Track
{
private $lapLength; //довжина треку, в км
private $lapsNumber; //кількість кіл)
private $cars = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        //@todo
        if($lapLength < 0) throw new \Exception('Длинна отрицательная');
        $this->lapLength = $lapLength;
        if($lapsNumber < 0) throw new \Exception('яколичество кругов  отрицательное');
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        // @todo
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        // @todo
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        // @todo
        $this->cars[] = $car;
    }

    public function all(): array
    {
        // @todo
        if(count($this->cars)==0) throw new \Exception('Отсутствуют автомобили');
        return $this->cars;
    }

    public function run(): Car
    {
        // @todo
        $allAutos = $this->all();

        $path = $this->getLapLength() * $this->getLapsNumber();
        $temtTime =0;
        $auto = [];

        foreach ( $allAutos as $key => $car ) {
            //Длинна пути на полном баке бек дозаправки
            $path_full_tank = ($car->getFuelTankVolume() * 100) / $car->getFuelConsumption();
            if ($path <= $path_full_tank) { //не нужно дозаправляться
                $t = (float)$temtTime +($path / $car->getSpeed());
                $n = $car->getName();
                $auto[]=['time'=>$t,'name'=>$n];

            } else
            { //путь с дозаправкой
                while ($path > $path_full_tank) {
                    $t  =    $temtTime + ($path_full_tank / $car->getSpeed()) +  ($car->getPitStopTime() / 3600);
                    $path = $path - $path_full_tank;
                    $temtTime = $t;
                    $n = $car->getName();
                    $auto[]=['time'=>$t,'name'=>$n];
                }//остаток
                if ($path < $path_full_tank) {
                    $t  = $temtTime  + ($path / $car->getSpeed());
                    $n = $car->getName();
                    $auto[]=['time'=>$t,'name'=>$n];
                }
            }
        }
        //поиск минимального времени заезда
        usort($auto, function($a, $b) {

            return $a['time'] <=> $b['time'];

        });
        $key = array_search($auto[0]['name'], $this->cars); // $key = 2;

        return $this->cars[$key];

    }
}