<?php

declare(strict_types=1);

namespace App\Task2;
use App\Task2\BooksGenerator;
class Book
{
  private $title;
  private $price;
  private $pagesNumber;

    /**
     * Book constructor.
     * @param $title
     * @param $prise
     * @param $pagesNumber
     */
    public function __construct($title=null, $price , $pagesNumber = null)
    {
        $this->title = $title;
        if($price<0)   throw new \Exception('Цена книги  не может быть отрицательной');
        $this->price = $price;
        if($pagesNumber<0)   throw new \Exception('Количество страниц  не может быть отрицательным');
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        // @todo
            return $this->title;
    }

    public function getPrice(): int
    {
        // @todo
            return $this->price;
    }

    public function getPagesNumber(): int
    {
        //@todo
            return $this->pagesNumber;
    }
}