<?php

declare(strict_types=1);

namespace App\Task2;
class BooksGenerator
{
private $minPagesNumber;
private $libraryBooks=[];
private $maxPrice;
private $storeBooks=[];
private $filteredBooks =[];

    /**
     * BooksGenerator constructor.
     * @param $minPagesNumber
     * @param $libraryBooks
     * @param $maxPrice
     * @param $storeBooks
     * @param $filteredBooks
     */
    //Без цього конструктора  другий тест валиться  не добавляэ  быблыотеку книг
    public function __construct($minPagesNumber, $libraryBooks, $maxPrice, $storeBooks)
    {
        if($minPagesNumber<0)   throw new \Exception('Количество страниц не может быть отрицательным');
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        if($maxPrice<0)   throw new \Exception('Цена книги не может быть отрицательной');
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }


    public function generate(): \Generator
    {
        //@todo
        foreach ($this->libraryBooks as $libBook) {
            if ( $libBook->getPagesNumber()>= $this->minPagesNumber) {
                $this->filteredBooks[] = $libBook;
            }
        }
        foreach ($this->storeBooks as $stBook) {
            if ((int)$stBook->getPrice()<= (int)$this->maxPrice ) {
                $this->filteredBooks[] = $stBook;
            }
        }
        foreach ($this->filteredBooks as $flBook) {
            yield $flBook;
        }
    }
}